export function drawCanvasCirclePartCustom (color, context, circlePart, opacity = 1) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.fill(circlePart)
}

export function drawCanvasCircleCustom (color, context, circle, opacity = 1) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.fill(circle)
}

export function drawCanvasCircleBorderCustom (centerX, centerY, radius, color, lineWidth, context, opacity = 1) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = lineWidth
  context.beginPath()
  context.arc(centerX, centerY, radius, 0, Math.PI * 2)
  context.stroke()
}

export function getCanvasCirclePart (centerX, centerY, radius, angle, height = 0, rotation = 0) {
  let circlePart = new Path2D()
  if (height === 0) {
    circlePart.moveTo(centerX, centerY)
    circlePart.arc(centerX, centerY, radius, rotation, rotation + angle)
    circlePart.closePath()
  } else {
    circlePart.arc(centerX, centerY, radius - height, rotation, rotation + angle)
    circlePart.arc(centerX, centerY, radius, rotation + angle, rotation, true)
    circlePart.closePath()
  }
  return circlePart
}

export function getCanvasCircle (centerX, centerY, radius) {
  let circle = new Path2D()
  circle.arc(centerX, centerY, radius, 0, Math.PI * 2)
  return circle
}

export function drawCanvasLineCustom (startX, startY, color, width, cap, dash, context, opacity, ...points) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = width
  context.lineCap = cap
  context.setLineDash(dash)
  context.beginPath()
  context.moveTo(startX, startY)
  for (let i = 0; i < points.length / 2; i++) {
    if (points[i * 2] !== undefined && points[i * 2 + 1] !== undefined) {
      context.lineTo(points[i * 2], points[i * 2 + 1])
    } else {
      break
    }
  }
  context.stroke()
}

export function drawCanvasTextCustom (x, y, text, color, align, baseline, font, context, opacity = 1) {
  context.textAlign = align
  context.textBaseline = baseline
  context.font = font
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.fillText(text, x, y)
}

export function drawCanvasCircle (centerX, centerY, radius, color, context, opacity = 1) {
  context.fillStyle = `rgba(${color}, ${opacity})`
  context.beginPath()
  context.arc(centerX, centerY, radius, 0, Math.PI * 2)
  context.fill()
}

export function drawCanvasCircleBorder (centerX, centerY, radius, color, width, cap, dash, context, opacity = 1) {
  context.strokeStyle = `rgba(${color}, ${opacity})`
  context.lineWidth = width
  context.lineCap = cap
  context.setLineDash(dash)
  context.beginPath()
  context.arc(centerX, centerY, radius, 0, Math.PI * 2)
  context.stroke()
}

import { createRouter, createWebHistory } from 'vue-router'
import TheMenu from '@/views/TheMenu.vue'
import TheGame from '@/views/TheGame.vue'
import TheStatistics from '@/views/TheStatistics.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'menu',
      component: TheMenu
    },
    {
      path: '/game',
      name: 'game',
      component: TheGame
    },
    {
      path: '/statistics',
      name: 'statistics',
      component: TheStatistics
    }
  ]
})

export default router

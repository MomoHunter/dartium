import { defineStore } from 'pinia'
import { useAppDynStore } from './appdyn'

export const useSavestateStore = defineStore({
  id: 'savestate',
  state: () => ({
    lang: 'german',
    profiles: [],
    allowUpdates: true,
    missedUpdates: false,
    updateSuccessful: false
  }),
  getters: {
    getFreeId: (state) => {
      let highestId = 1
      for (let profile of state.profiles) {
        if (profile.id >= highestId) {
          highestId = profile.id + 1
        }
      }
      return highestId
    },
    getProfileById: (state) => (id) => {
      return state.profiles.find(profile => profile.id === id) // pass by reference
    },
    getGameByProfileId: (state) => (id, pos = -1) => {
      const savestate = useSavestateStore()
      const profile = savestate.getProfileById(id)
      if (pos === -1) {
        pos = profile.games.length - 1
      }
      return profile.games[pos] // pass by reference
    }
  },
  actions: {
    saveData () {
      window.localStorage.setItem('dartiumGD', JSON.stringify(this.$state))
    },
    loadData () {
      const savestate = useSavestateStore()
      const appDyn = useAppDynStore
      let data = window.localStorage.getItem('dartiumGD')

      if (data) {
        data = JSON.parse(data)

        if (Object.hasOwn(data, 'updateSuccessful') && data.updateSuccessful) {
          if (navigator.serviceWorker) {
            navigator.serviceWorker.getRegistration().then((registration) => {
              if (registration && registration.waiting) {
                savestate.updateSuccessful = true
              } else {
                appDyn.updateSuccess = true
                savestate.saveData()
              }
            })
          }
        }

        if (data.profiles) {
          savestate.profiles = data.profiles
        }
      }
    },
    deleteData () {
      window.localStorage.removeItem('dartiumGD')
    },
    resetData () {
      const savestate = useSavestateStore()
      
      savestate.lang = 'german'
      savestate.profiles = []
    },
    deleteProfilesByIds (ids) {
      const savestate = useSavestateStore()

      savestate.profiles = savestate.profiles.filter(profile => !ids.includes(profile.id))
    }
  }
})
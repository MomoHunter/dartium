import { defineStore } from 'pinia'
import { useSavestateStore } from './savestate'

const textObj = import.meta.globEager('../data/translations/*.json')

export const useAppConstStore = defineStore({
  id: 'appconst',
  state: () => ({
    texts: Object.fromEntries(
      Object.entries(textObj).map(entry => {
        entry[0] = entry[0].substring(
          entry[0].lastIndexOf('/') + 1,
          entry[0].lastIndexOf('.')
        )
        return entry
      })
    ),
    points: [
      501,
      301,
      180,
      401,
      601,
      701,
      801,
      901,
      1001
    ],
    variants: [
      'straightOut',
      'doubleOut',
      'masterOut',
      'doubleIn',
      'tripleIn'
    ],
    colors: [
      'green-dark',
      'green',
      'green-light',
      'red-dark',
      'red',
      'red-light',
      'blue-dark',
      'blue',
      'blue-light',
      'yellow-dark',
      'yellow',
      'yellow-light',
      'magenta-dark',
      'magenta',
      'magenta-light'
    ],
    icons: [
      'hippo',
      'chess',
      'user',
      'star',
      'music',
      'bomb',
      'heart',
      'camera-retro',
      'bell',
      'poo',
      'gift',
      'car',
      'ghost',
      'film',
      'camera',
      'plane',
      'fire',
      'key',
      'lemon',
      'money-bill',
      'truck',
      'bicycle',
      'snowflake',
      'flask',
      'anchor',
      'bug',
      'code',
      'landmark',
      'feather',
      'sun',
      'gamepad',
      'fish',
      'palette',
      'cross',
      'flag',
      'rocket',
      'leaf',
      'motorcycle',
      'train',
      'dragon',
      'truck-monster',
      'joint',
      'hammer',
      'mask'
    ]
  }),
  getters: {
    /**
     * automatically translates text with a text id
     * @param {Object} state state of pinia store component (internal)
     * @param {String} id the text id
     * @param {Any} params parameters that should get inserted into the text at & positions
     */
    getText: (state) => (id, ...params) => {
      const savestate = useSavestateStore()
      const appConst = useAppConstStore()
      if (isNaN(id)) {
        let text = state.texts[savestate.lang][id]
        if (text) {
          if (params.length > 0) {
            for (let i = 0; i < params.length; i++) {
              let regex = new RegExp('&' + (i + 1), 'g')
              text = text.replace(regex, appConst.getText(params[i]))
            }
          }
          return text
        } else {
          return id
        }
      } else {
        return id.toString()
      }
    },
  },
  actions: {}
})
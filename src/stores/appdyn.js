import { defineStore } from 'pinia'

export const useAppDynStore = defineStore({
  id: 'appdyn',
  state: () => ({
    players: [],
    points: 501,
    variant: 'singleOut',
    winner: null,
    newUpdate: false,
    updateFinished: false,
    updateAvailable: false,
    updateSuccess: false
  }),
  getters: {},
  actions: {}
})
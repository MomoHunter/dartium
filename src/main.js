import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import './registerServiceWorker'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.directive('square', {
  mounted (el) {
    if (el.offsetWidth < el.offsetHeight) {
      el.style.width = el.offsetWidth + 'px'
      el.style.height = el.offsetWidth + 'px'
    } else {
      el.style.width = el.offsetHeight + 'px'
      el.style.height = el.offsetHeight + 'px'
    }
  }
})

app.directive('fullsize', {
  mounted (el) {
    let boundingBox = el.parentElement.getBoundingClientRect()
    if (boundingBox.width < boundingBox.height) {
      el.setAttribute('width', boundingBox.width)
      el.setAttribute('height', boundingBox.width)
    } else {
      el.setAttribute('width', boundingBox.height)
      el.setAttribute('height', boundingBox.height)
    }
  }
})

app.directive('fullsize2', {
  mounted (el) {
    let boundingBox = el.parentElement.getBoundingClientRect()
    el.setAttribute('width', boundingBox.width)
    el.setAttribute('height', boundingBox.height)
  }
})

app.mount('#app')
